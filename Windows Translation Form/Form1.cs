﻿using MenuSync;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Windows_Translation_Form
{
    public partial class Form1 : Form
    {
        List<ProductTranslation> glb_translations = new List<ProductTranslation>();

        public Form1()
        {
            VertedaDiagnostics.OpenLog(ConfigurationManager.AppSettings["LOG_Directory"], "VTI Reporting.log");

            VertedaDiagnostics.LogMessage("=======================LOAD=======================", "I", false);
            VertedaDiagnostics.LogMessage("VTI Reporting Started", "I", false);

            InitializeComponent();

            // option of left to right or right to left
            if (ConfigurationManager.AppSettings["RightToLeft"] == "1")
            {
                textBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            }
            else
            {
                textBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            }

            //load existing xml
            glb_translations = ReadXML();

            //load products list - any already in xml as red
            comboBox1.Items.Clear();
            foreach (ComboboxItem cbi in LoadProducts())
            {
                comboBox1.Items.Add(cbi);
            }
            comboBox1.SelectedIndex = 0;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //close
            this.Close();
        }

        private void SaveXML()
        {
            XDocument doc = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XElement("Products",
                    from ts in glb_translations
                    select new XElement("Product", null,
                           new XElement("ID", ts.ID),
                           new XElement("Name", ts.Product),
                           new XElement("Translation", ts.Translation))
                    )
                );
            doc.Save(ConfigurationManager.AppSettings["XML_Path"]);
            VertedaDiagnostics.LogMessage("XML file saved", "I", false);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //save
            ProductTranslation cur_trans = new ProductTranslation();
            ComboboxItem def_combo = comboBox2.Items[comboBox2.SelectedIndex] as ComboboxItem;

            var obj = glb_translations.FirstOrDefault(p => p.ID == def_combo.Value);

            if (textBox1.Text != "")
            {
                if (obj != null)
                {
                    //amend    
                    obj.Translation = textBox1.Text;
                    VertedaDiagnostics.LogMessage(comboBox1.Text + " " + textBox1.Text + " amended", "I", false);
                }
                else
                {
                    //add
                    cur_trans.ID = def_combo.Value;
                    cur_trans.Product = def_combo.Text;
                    cur_trans.Translation = textBox1.Text;

                    glb_translations.Add(cur_trans);
                    VertedaDiagnostics.LogMessage(comboBox1.Text + " " + textBox1.Text + " added", "I", false);
                    comboBox1.Invalidate();
                }
            }
            else
            {
                if (obj != null)
                {
                    //amend    
                    DialogResult dg = MessageBox.Show("Do you want to delete this translation?", "Confirm Delete", MessageBoxButtons.YesNo);
                    if (dg == DialogResult.Yes)
                    {
                        VertedaDiagnostics.LogMessage(obj.Product + " " + obj.Translation + " removed", "I", false);
                        glb_translations.Remove(obj);
                        comboBox1.Invalidate();
                    }
                }
                else
                    MessageBox.Show("Please enter a translation before saving");
            }
            textBox1.BackColor = SystemColors.Window;
        }

        private List<ComboboxItem> LoadProducts()
        {
            List<ComboboxItem> product_list = new List<ComboboxItem>();
            List<ComboboxItem> default_list = new List<ComboboxItem>();

            SqlConnection conn = new SqlConnection(GetConnectionString());

            if (conn.State != ConnectionState.Open)
                try
                {
                    conn.Open();
                    VertedaDiagnostics.LogMessage("SQL Connection Open", "I", false);
                }
                catch (SqlException e)
                {
                    MessageBox.Show(e.ToString());
                    VertedaDiagnostics.LogMessage("SQL Connection Error", "I", false);
                }

            if (conn.State == ConnectionState.Open)
            {
                SqlCommand cmd = new SqlCommand(ConfigurationManager.AppSettings["MENU_ITEM_Query"], conn);
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    // if fields queried from database increases, update string array size.
                    string[] rowValues = new string[3];
                    for (int i = 0; i < rdr.FieldCount; i++)
                    {
                        if (rdr.IsDBNull(i))
                        {
                            rowValues[i] = "NULL"; // LOG THAT VALUE IS NULL
                        }
                        else
                            rowValues[i] = rdr.GetValue(i).ToString();
                    }

                    ComboboxItem prod_item = new ComboboxItem();
                    prod_item.Value = rowValues[0];
                    prod_item.Text = rowValues[1];

                    product_list.Add(prod_item);

                    ComboboxItem def_item = new ComboboxItem();
                    def_item.Value = rowValues[0];
                    def_item.Text = rowValues[2];
                    
                    comboBox2.Items.Add(def_item);
                }
                rdr.Close();
                conn.Close();
            }            
            return product_list;
        }

        public static string GetConnectionString()
        {
            string connStr;
            //// get mysql connection details from config file. 
                connStr = String.Format("Server={0};Uid={1};Pwd={2};Database={3}; Integrated Security=false",
                                 ConfigurationManager.AppSettings["MENU_ITEM_IP"],
                                 ConfigurationManager.AppSettings["MENU_ITEM_User"],
                                 ConfigurationManager.AppSettings["MENU_ITEM_Password"],
                                 ConfigurationManager.AppSettings["MENU_ITEM_Database"]);
            return connStr;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //reset colour when index changes
            textBox1.BackColor = SystemColors.Window;

            ComboboxItem prod_combo = comboBox1.Items[comboBox1.SelectedIndex] as ComboboxItem;
            string prod_id = prod_combo.Value;

            //find id of changed CBI. set CB2 index = matching ID
            if (comboBox1.SelectedIndex != 0)
            {                
                foreach (ComboboxItem cbi in comboBox2.Items)
                {
                    if (cbi.Value == prod_id)
                        comboBox2.SelectedIndex = comboBox2.Items.IndexOf(cbi);
                }
            }
            else
            {
                comboBox2.SelectedIndex = 0;
            }

            //check to see if translation already exists
            var match = glb_translations.Where(p => p.ID == prod_id).ToList();

            if (match.Count > 0)
            {
                textBox1.Text = match[0].Translation;
            }
            else
            {
                textBox1.Text = "";
            }
        }

        private List<ProductTranslation> ReadXML()
        {
            List<ProductTranslation> li_translations = new List<ProductTranslation>();            

            if (!File.Exists(ConfigurationManager.AppSettings["XML_Path"]))
            {
                //create new
                MessageBox.Show("XML config not found");
                VertedaDiagnostics.LogMessage("XML translations file missing", "I", false);
            }
            else
            {
                //open existing
                VertedaDiagnostics.LogMessage("XML translations file loaded", "I", false);
                XDocument doc = XDocument.Load(ConfigurationManager.AppSettings["XML_Path"]);

                foreach (XElement el in doc.Root.Elements())
                {                        
                    ProductTranslation PT = new ProductTranslation();
                    PT.ID = el.Element("ID").Value;
                    PT.Product = el.Element("Name").Value;
                    PT.Translation = el.Element("Translation").Value;

                    li_translations.Add(PT);                    
                }
            }
            return li_translations;
        }

        private void comboBox1_DrawItem(object sender, DrawItemEventArgs e)
        {
            // Draw the background 
            e.DrawBackground();

            // Get the item ID    
            ComboBox CB = (ComboBox)sender;
            ComboboxItem CBI = (ComboboxItem)CB.Items[e.Index];
            string value = CBI.Value;

            var matches = glb_translations.Where(p => p.ID == value).ToList();

            string text = ((ComboBox)sender).Items[e.Index].ToString();

            // Determine the forecolor based on whether or not the item is selected    
            Brush brush;

            if (matches.Count > 0)// compare  date with your list.  
                
            {
                brush = Brushes.Crimson;
            }
            else
            {
                brush = Brushes.Black;
            }

            // Draw the text    
            e.Graphics.DrawString(text, ((Control)sender).Font, brush, e.Bounds.X, e.Bounds.Y);
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (textBox1.BackColor == SystemColors.Window)
            {
                textBox1.BackColor = Color.Khaki;
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (textBox1.BackColor == SystemColors.Window)
            {
                textBox1.BackColor = Color.Khaki;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //close
            DialogResult r1 = MessageBox.Show("Do you want to save changes?", "Save and Quit", MessageBoxButtons.YesNoCancel);

            if (r1 == DialogResult.Yes)
            {
                //save & close
                SaveXML();
                VertedaDiagnostics.LogMessage("=======================QUIT=======================", "I", false);
                VertedaDiagnostics.CloseLog();
            }
            else if (r1 == DialogResult.No)
            {
                //close
                VertedaDiagnostics.LogMessage("=======================QUIT=======================", "I", false);
                VertedaDiagnostics.CloseLog();
            }
            else
            {
                //cancel the close
                e.Cancel = true;
            }
            //else do nothing
        }
     }

    public class ComboboxItem
    {
        public string Text { get; set; }
        public string Value { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }

    public class ProductTranslation
    {
        public string Product { get; set; }
        public string Translation { get; set; }
        public string ID { get; set; }
    }
}
